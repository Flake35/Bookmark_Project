import React from "react";

export class AddingForm extends React.Component<{action: any}, {url: string, keyWord: string}> {

    constructor(props: any) {
        super(props);
        this.state = {url: '', keyWord: ''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event: any) {
        switch (event.target.name) {
            case "url":
                this.setState({url: event.target.value});
                break;
            case "keyWord":
                this.setState({keyWord: event.target.value});
                break;
            default:
                break;
        }
    }

    handleSubmit(event: any) {
        this.props.action(this.state.url, this.state.keyWord);
        event.preventDefault();
    }

    render() {
        return (
            <div className="component">
                <h2>Formulaire d'ajout de lien</h2>

                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label>Lien</label>
                        <input className="form-control form-control-lg" type="text" name="url" value={this.state.url} onChange={this.handleChange} />
                    </div>
                    <div className="form-group">
                        <label>Mots clés</label>
                        <textarea className="form-control form-control-lg" name="keyWord" value={this.state.keyWord} onChange={this.handleChange} />
                    </div>
                    <button type="submit" className="btn btn-success btn-block">Ajouter</button>
                </form>
            </div>
        );
    }
}