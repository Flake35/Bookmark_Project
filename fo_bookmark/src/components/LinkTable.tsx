import React from "react";
import {Bookmark} from "../models/Bookmark";
import {Lien} from "../models/Lien";

export class LinkTable extends React.Component<{bookmark: Bookmark, remove: any, modify: any}, {}> {

    constructor(props: any) {
        super(props);

        this.handleRemoveClic = this.handleRemoveClic.bind(this);
        this.handleModifyCLic = this.handleModifyCLic.bind(this);
    }

    handleRemoveClic(titre: string) {
        this.props.remove(titre);
    }

    handleModifyCLic(lien: Lien) {
        this.props.modify(lien);
    }

    render() {
        let content = this.props.bookmark.map((lien, index) =>
            <tr key={index}>
                <th>{index+1}</th>
                <td>{lien.oEmbedInfo.title}</td>
                <td>{lien.oEmbedInfo.author_name}</td>
                <td><a target="_blank" rel="noopener noreferrer" href={lien.url}>{lien.url}</a></td>
                <td>{lien.oEmbedInfo.date}</td>
                <td>{lien.key_words}</td>
                <td onClick={() => {this.handleRemoveClic(lien.url)}}>
                    <svg className="bi bi-trash2-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M2.037 3.225l1.684 10.104A2 2 0 005.694 15h4.612a2 2 0 001.973-1.671l1.684-10.104C13.627 4.224 11.085 5 8 5c-3.086 0-5.627-.776-5.963-1.775z"/>
                        <path fillRule="evenodd" d="M12.9 3c-.18-.14-.497-.307-.974-.466C10.967 2.214 9.58 2 8 2s-2.968.215-3.926.534c-.477.16-.795.327-.975.466.18.14.498.307.975.466C5.032 3.786 6.42 4 8 4s2.967-.215 3.926-.534c.477-.16.795-.327.975-.466zM8 5c3.314 0 6-.895 6-2s-2.686-2-6-2-6 .895-6 2 2.686 2 6 2z" clipRule="evenodd"/>
                    </svg>
                </td>
                <td onClick={() => {this.handleModifyCLic(lien)}}>
                    <svg className="bi bi-pencil" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fillRule="evenodd" d="M11.293 1.293a1 1 0 011.414 0l2 2a1 1 0 010 1.414l-9 9a1 1 0 01-.39.242l-3 1a1 1 0 01-1.266-1.265l1-3a1 1 0 01.242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z" clipRule="evenodd"/>
                        <path fillRule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 00.5.5H4v.5a.5.5 0 00.5.5H5v.5a.5.5 0 00.5.5H6v-1.5a.5.5 0 00-.5-.5H5v-.5a.5.5 0 00-.5-.5H3z" clipRule="evenodd"/>
                    </svg>
                </td>
            </tr>
        )

        return (
            <div className="component">
                <h2>Liste des liens sauvegardés</h2>
                <table className="table table-striped table-hover">
                    <thead className="thead-dark">
                    <tr>
                        <th scope='col'>#</th>
                        <th scope='col'>Titre</th>
                        <th scope='col'>Auteur</th>
                        <th scope='col'>Url</th>
                        <th scope='col'>Date</th>
                        <th scope='col'>Mots clés</th>
                        <th scope='col'>Supprimer</th>
                        <th scope='col'>Modifier</th>
                    </tr>
                    </thead>
                    <tbody>
                    {content}
                    </tbody>
                </table>
            </div>
        );
    }
}