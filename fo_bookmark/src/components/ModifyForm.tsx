import React from "react";
import {Lien} from "../models/Lien";

export class ModifyForm extends React.Component<{ display: boolean, close: any, link: Lien , modify: any}, {keyWord: string}> {
    constructor(props: any) {
        super(props);
        this.state = {keyWord: this.props.link.key_words};

        this.closeForm = this.closeForm.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event: any) {
        this.setState({keyWord: event.target.value});
    }

    closeForm() {
        this.props.close();
    }

    handleSubmit(event: any) {
        this.props.modify(this.props.link.url, this.state.keyWord);
        this.props.close();
        event.preventDefault();
    }

    render() {
        return (
            <div className="component">
                <h2>Modification du lien : {this.props.link.oEmbedInfo.title} ({this.props.link.url})</h2>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label>Mots clés</label>
                        <input className="form-control" type="text" name="keyWord" value={this.state.keyWord} onChange={this.handleChange} />
                    </div>
                    <button type="submit" className="btn btn-outline-success btn-block">Valider</button>
                    <button className="btn btn-outline-danger btn-block" onClick={this.closeForm}>Fermer</button>
                </form>
            </div>
        );
    }
}