import {OembedInfo} from "./OembedInfo";

export class OembedInfoVimeo extends OembedInfo {
    duration: number;

    constructor(title: string, author_name: string, width: number, height: number, upload_date: string, duration: number) {
        super(title, author_name, width, height, upload_date);
        this.duration = duration;
    }
}