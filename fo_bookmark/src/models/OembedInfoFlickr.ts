import {OembedInfo} from "./OembedInfo";

export class OembedInfoFlickr extends OembedInfo {
    constructor(title: string, author_name: string, width: number, height: number, upload_date: string) {
        super(title, author_name, width, height, upload_date);
    }
}