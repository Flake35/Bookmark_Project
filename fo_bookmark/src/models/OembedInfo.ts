export class OembedInfo {
    title: string;
    author_name: string;
    width: number;
    height: number;
    date: string;

    constructor(title: string, author_name: string, width: number, height: number, date: string) {
        this.title = title;
        this.author_name = author_name;
        this.width = width;
        this.height = height;
        this.date = date;
    }
}