import {OembedInfo} from "./OembedInfo";

export class Lien {
    url: string;
    key_words: string;
    oEmbedInfo: OembedInfo;

    constructor(url: string, keyWord: string, oembedInfo: OembedInfo){
        this.url = url;
        this.key_words = keyWord;
        this.oEmbedInfo = oembedInfo;
    };
}