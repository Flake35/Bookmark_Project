import {Bookmark} from "../models/Bookmark";

const BookmarkApi = "http://localhost:5000/bookmark"

export class BookMarkService {
    bookmark: Bookmark;

    constructor() {
        this.bookmark = new Bookmark();
    }

    getBookmark(updateState: any) {
        fetch(BookmarkApi)
            .then(res => res.json())
            .then((data) => {
                this.bookmark = data;
                updateState(this.bookmark)
            })
            .catch(console.log);
    }

    addLink(url: string, keyWord: string, type: string, updateState: any) {
        fetch(BookmarkApi+'/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ url: url, key_word: keyWord, type: type }),
        })
            .then(res => res.json())
            .then((res) => {
            if (!res.added) {
                alert("Imposible d'ajouter le lien.\nVeuillez vérifier que le lien existe.")
            } else {
                this.getBookmark(updateState)
            }
        });
    }

    delLink(url: string, updateState: any) {
        fetch(BookmarkApi+'/delete', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ url: url }),
        }).then(() => this.getBookmark(updateState));
    }

    modLink(url: string, keyWord: string, updateState: any) {
        fetch(BookmarkApi+'/modify', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ url: url, key_word: keyWord }),
        }).then(() => this.getBookmark(updateState));
    }
}