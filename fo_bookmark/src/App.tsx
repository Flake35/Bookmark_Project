import React from 'react';
import './App.css';
import {Lien} from "./models/Lien";
import {Bookmark} from "./models/Bookmark";
import {LinkTable} from "./components/LinkTable";
import {AddingForm} from "./components/AddingForm";
import {ModifyForm} from "./components/ModifyForm";
import {OembedInfo} from "./models/OembedInfo";
import {BookMarkService} from "./services/BookMarkService";

const UrlRegexVimeo = new RegExp(".*(vimeo.com).*");
const UrlRegexFlickr = new RegExp(".*(flickr.com).*");
const BookmarkService = new BookMarkService();

class App extends React.Component<{}, {bookMark: Bookmark, display: boolean, currentLink: Lien}> {

    constructor(props: any) {
        super(props);

        this.state = {
            bookMark: new Bookmark(),
            display: false,
            currentLink: new Lien('', '', new OembedInfo('', '', 0, 0, ''))};
        this.addLinkToBookmark = this.addLinkToBookmark.bind(this);
        this.delLinkToBookmark = this.delLinkToBookmark.bind(this);
        this.modLinkToBookMark = this.modLinkToBookMark.bind(this);
        this.hideModification = this.hideModification.bind(this);
        this.handleModification = this.handleModification.bind(this);
        this.updateState = this.updateState.bind(this);
    }

    componentDidMount() {
        BookmarkService.getBookmark(this.updateState);
    }

    updateState(bookmark: Bookmark) {
        this.setState({bookMark: bookmark});
    }

    async addLinkToBookmark(url: string, keyWord: string) {
        let isAlreadyIn : boolean = false;
        this.state.bookMark.forEach(currentLink => {if(url === currentLink.url) isAlreadyIn = true})
        if (isAlreadyIn) {
            alert("Le lien est déjà présent dans la liste.");
        } else if (url.match(UrlRegexVimeo)) {
            BookmarkService.addLink(url, keyWord, "vimeo", this.updateState);
        } else if (url.match(UrlRegexFlickr)) {
            BookmarkService.addLink(url, keyWord, "flickr", this.updateState);
        }
    }

    delLinkToBookmark(url: string) {
        BookmarkService.delLink(url, this.updateState);
    }

    modLinkToBookMark(lien: Lien) {
        this.setState({display: true, currentLink: lien})
    }

    hideModification() {
        this.setState({display: false})
    }

    handleModification(url: string, keyWords: string) {
        BookmarkService.modLink(url, keyWords, this.updateState);
    }

    render() {
        return (
            <div className="App">
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
                      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
                      crossOrigin="anonymous"/>
                <header className="App-header">
                    <h1>Bienvenue sur votre application de bookmark</h1>
                    <p>Ici vous pouvez ajouter ou modifier des liens (Vimeo ou Flickr) et visualiser les liens sauvegardés.</p>
                </header>
                <div className="App-body">
                    <LinkTable bookmark={this.state.bookMark} remove={this.delLinkToBookmark} modify={this.modLinkToBookMark}/>
                    <AddingForm action={this.addLinkToBookmark}/>
                    {this.state.display && <ModifyForm display={this.state.display} close={this.hideModification} link={this.state.currentLink} modify={this.handleModification}/>}
                </div>
            </div>
        );
    }
}

export default App;
