class OembedInfo {

    constructor(title, author_name, width, height, date, duration) {
        this.title = title;
        this.author_name = author_name;
        this.width = width;
        this.height = height;
        this.date = date;
        this.duration = duration;
    }
}

module.exports = OembedInfo;