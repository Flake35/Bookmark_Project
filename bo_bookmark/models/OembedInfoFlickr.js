import {OembedInfo} from "./OembedInfo";

export class OembedInfoFlickr extends OembedInfo {

    constructor(title, author_name, width, height) {
        super(title, author_name, width, height);
    }
}