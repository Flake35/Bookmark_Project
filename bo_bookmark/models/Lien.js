class Lien {

    constructor(url, keyWord, oembedInfo){
        this.url = url;
        this.key_words = keyWord;
        this.date = new Date().toLocaleDateString();
        this.oEmbedInfo = oembedInfo;
    };
}

module.exports = Lien;