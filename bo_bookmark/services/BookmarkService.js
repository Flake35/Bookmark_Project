class BookmarkService {

    constructor() {
        this.bookMark = [];
    }

    getBookmark() {
        return this.bookMark;
    }

    addLink(lien) {
        this.bookMark.push(lien);
    }

    delLink(url) {
        this.bookMark = this.bookMark.filter(lien => lien.url !== url);
    }

    modLink(url, keywords) {
        this.bookMark.forEach(lien => {
            if(lien.url === url) {
                lien.key_words = keywords;
            }
        });
    }
}

module.exports = BookmarkService;