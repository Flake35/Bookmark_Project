const https = require('https');

const OembedInfoVimeoApi = "https://vimeo.com/api/oembed.json?url=";
const OembedInfoFlickrApi = "https://www.flickr.com/services/oembed/?format=json&url=";

class OEmbedService {

    getInfoVimeoLink(url) {
        return this.getInfoOembed(OembedInfoVimeoApi, url);
    }

    getInfoFlickrLink(url) {
        return this.getInfoOembed(OembedInfoFlickrApi, url);
    }

    getInfoOembed(apiUrl, url) {
        return new Promise((resolve, reject) => {
            https.get(apiUrl + url, (resp) => {
                let infos = '';

                resp.on('data', (chunk) => {
                    infos += chunk;
                });

                resp.on('end', () => {
                    try {
                        resolve(JSON.parse(infos));
                    } catch (e) {
                        reject(e);
                    }
                });
            }).on("error", (err) => {
                console.log("Error: " + err.message);
            });
        });
    }
}

module.exports = OEmbedService;