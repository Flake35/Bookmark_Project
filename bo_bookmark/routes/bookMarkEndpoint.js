const BookmarkService = require('../services/BookmarkService');
const OEmbedService = require('../services/OEmbedService');
const Lien = require('../models/Lien');
const OembedInfo = require('../models/OembedInfo');

var express = require("express");
var router = express.Router();

const bookmarkService = new BookmarkService();
const oEmbedService = new OEmbedService();

router.get("/", function(req, res) {
    res.send(bookmarkService.getBookmark());
});

router.post("/add", function(req, res) {
    if(req.body.type === "vimeo") {
        oEmbedService.getInfoVimeoLink(req.body.url)
            .then((info) => {
                bookmarkService.addLink(
                    new Lien(req.body.url, req.body.key_word, new OembedInfo(info.title, info.author_name, info.width, info.height, info.upload_date, info.duration))
                );
            }).then( () => {
                res.send({added: true});
            }).catch((err) => {
                console.log(err);
                res.send({added: false});
            });
    } else {
        oEmbedService.getInfoFlickrLink(req.body.url)
            .then((info) => {
                bookmarkService.addLink(
                    new Lien(req.body.url, req.body.key_word, new OembedInfo(info.title, info.author_name, info.width, info.height))
                );
            }).then( () => {
                res.send({added: true});
            }).catch((err) => {
                console.log(err);
                res.send({added: false});
            });
    }
});

router.post("/delete", function(req, res) {
    bookmarkService.delLink(req.body.url);
    res.send({del: true});
});

router.post("/modify", function(req, res) {
    bookmarkService.modLink(req.body.url, req.body.key_word);
    res.send({mod: true});
});

module.exports = router;