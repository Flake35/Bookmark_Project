# Bookmark Project

Ce projet présente une application de gestion de liens référencés.

# Guide d'installation

Pour lancer le projet, il vous faut avoir au préalable **git** et **node** d'installé.

## Récupération du projet

Pour récupérer le projet, il vous faut utiliser la commande **git clone** dans le répertoire où vous souhaitez copier le projet

>  git clone https://gitlab.com/Flake35/Bookmark_Project.git

## Lancement du front

L'application react pour le front est placée dans le répertoire **fo_bookmark**.

Une fois positionné dans le dossier, exécutez les commandes suivantes

>  npm install (pour installer les dépendances du projet)

>  npm start (pour lancer le projet)

L'application est accéssible à l'adresse http://localhost:3000/

## Lancement du back

En parallèle du lancement du front il vous faut lancer le back office pour avoir les api.

Le back est placé sous **bo_bookmark**.

Une fois dans le dossier, executez les commandes suivantes

>  npm install (pour installer les dépendances du projet)

>  npm start (pour lancer le projet)

## Nota bene

Pour que l'application fonctionne correctement, il faut veiller à ce que le front office et le back office soit tous les deux en cours d'exécution.